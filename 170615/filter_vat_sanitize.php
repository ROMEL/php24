<?php
$userInput = "Love the site. E-mail me at <a href='http://www.example.com'>Spammer</a>.";
echo "<h3>Before sanitization:</h3>".$userInput;
$sanitizedInput = filter_var($userInput, FILTER_SANITIZE_STRING);
echo "<h3>After sanitization:</h3>" .$sanitizedInput;
?>