-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2015 at 06:46 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `newsupershop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(30) DEFAULT NULL,
  `admin_password` varchar(30) DEFAULT NULL,
  `admin_name` varchar(50) DEFAULT NULL,
  `admin_email` varchar(50) DEFAULT NULL,
  `group` int(1) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin_id`, `admin_password`, `admin_name`, `admin_email`, `group`, `updated`, `created`) VALUES
(1, 'admin', 'nolimit', 'ASA AL-MAMUN', 'mamuncitiut@yahoo.com', 0, '2012-11-27 18:18:50', '2012-11-27 18:18:53'),
(6, 'mamun', 'mamun', 'No Limit', 'minimaxcom@gmail.com', 1, '2013-06-24 12:18:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categorytable`
--

CREATE TABLE IF NOT EXISTS `categorytable` (
  `categoryId` int(11) NOT NULL,
  `categoryName` varchar(30) NOT NULL,
  `categoryCreated` date NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorytable`
--

INSERT INTO `categorytable` (`categoryId`, `categoryName`, `categoryCreated`) VALUES
(1, 'Mens Ware', '2012-11-10'),
(2, 'Ladies Ware', '2012-11-10'),
(3, 'Electronics', '2012-11-10'),
(4, 'Mobile', '2012-12-27'),
(5, 'Computer', '2012-12-27'),
(6, 'Chocolate', '2012-12-27'),
(7, 'Cosmetic', '2012-12-27'),
(8, 'Food', '2012-12-27'),
(9, 'Grocery', '2012-12-27'),
(10, 'Biscuit', '2012-12-27'),
(11, 'Baby Food', '2012-12-27'),
(12, 'Toy', '2012-12-31'),
(13, 'Soft-Drinks', '2012-12-27'),
(14, 'Book', '2013-02-24'),
(15, 'Medicine', '2013-02-24'),
(16, 'Vehicle', '2013-03-25'),
(17, 'Baby set', '2013-06-09');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL,
  `stateId` int(11) NOT NULL,
  `city` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `stateId`, `city`) VALUES
(1, 1, 'Lucknow'),
(2, 1, 'Bareilly'),
(3, 2, 'Pithoragarh'),
(4, 2, 'Dehradun'),
(5, 2, 'Nainital');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL,
  `countryName` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `countryName`) VALUES
(1, 'India'),
(2, 'Bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customerID` int(10) NOT NULL,
  `firstName` varchar(10) NOT NULL,
  `lastName` varchar(10) NOT NULL,
  `phoneNumber` varchar(12) NOT NULL,
  `emailAddress` varchar(40) NOT NULL,
  `registered` int(1) NOT NULL DEFAULT '0',
  `password` varchar(40) CHARACTER SET utf8 NOT NULL,
  `address_1` varchar(30) NOT NULL,
  `address_2` varchar(30) NOT NULL,
  `country` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `city` varchar(30) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `comments` varchar(30) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerID`, `firstName`, `lastName`, `phoneNumber`, `emailAddress`, `registered`, `password`, `address_1`, `address_2`, `country`, `state`, `city`, `zip`, `comments`) VALUES
(1, 'Jillur', 'Rahman', '01912604728', 'jillur@yahoo.com', 1, '111111', 'dhaka', 'Pirojpur', '', '', '', '1216', 'New Customer'),
(2, 'Bashir', 'Raihan', '01723432165', 'raihan@yahoo.com', 0, '111111', 'asadgate, Dhaka', 'panchagarh', '2', '3', '10', '3456', 'New Customer'),
(4, 'D.M', 'Shahin', '01911561554', 'dm_shahin@yahoo.com', 1, 'shahin123', 'Subahanbag', 'Dhaka', '', '', '', '1208', 'New Cuatomar'),
(7, 'Mahmudul', 'Hoassan', '01912563302', 'hassan@yahoo.com', 0, 'hassan123', 'Dhaka', 'Jatrabari', '2', '3', '7', '1214', 'New Customer'),
(35, 'abdullah', 'al-mamun', '01911039525', 'mamun@gmail.com', 0, 'mamun', 'Mirpur, Section 7', 'Dhaka-1221', 'Bangladesh', 'Dhaka', 'Dhaka', '1221', 'New customer in 2012'),
(36, 'Al', 'Amin', '01718489837', 'ashikalamin21@yahoo.com', 0, 'ashik123', 'Lalbag', 'Barishal', '2', '4', '8', '1578', 'VIP Customer'),
(37, 'Tanzil', 'Alam', '01979398706', 'tanzil@gmail.com', 0, '', '', 'Barishal', '', '', '', '1200', 'Our VIP Customer'),
(38, 'Monira', 'Begum', '01720035688', 'monira@gmail.com', 0, 'monira123', 'Dhanmondi-15', 'Rangpur', '2', '3', '10', '1205', 'Our VIP Guest'),
(39, 'SP', 'Apon', '01670462007', 'apon@gmail.com', 1, 'apon123', 'Lalbag', 'Sirajgonj', '', '', '', '1204', 'New Customer'),
(40, 'J.M', 'Rifat', '+16464365082', 'jmrifat@yahoo.com', 0, 'rifat123', 'New York', 'Durgapur', '2', '3', '10', '1205', 'Our VIP Guest'),
(41, 'Manik', 'Banik', '01911637106', 'mcbanik@yahoo.com', 0, 'manik123', 'Jamalpur Ashek Mahmud College', 'Durgapur', '2', '3', '7', '1254', 'Teacher'),
(47, 'tanzil_ala', 'sorder', '+8801', 'tttttt', 0, '111111', '', '', '', '', '', '', ''),
(46, 'Dewan Moha', 'Shahin', '+88019115615', 'dmshahin@ymail.com', 1, '0', '', '', '', '', '', '', 'I want to your Customer'),
(49, 'Sefatul', 'Islam', '+88017124095', 'sefatulislam@yahoo.com', 0, '', '', '', '', '', '', '', ''),
(50, 'Alom', 'monzil', '+88019124678', 'alom@yahoo.com', 1, '11111', '', '', '', '', '', '', 'i want to be a customer'),
(58, 'abu', 'mamun', '01911039525', 'abu@gmail.com', 1, '12345', 'mirpur', 'asdf', '2', '3', '10', 'undefined', 'sadf'),
(57, 'Shek', 'Hasina', '01710000000', 'shekhasina@hotmail.com', 1, '12345', '', '', '', '', '', 'undefined', 'I want to Your Customer'),
(56, 'Hasan', 'Vi', '01912457896', 'hasan@yahoo.com', 1, '12345', '', '', '', '', '', 'undefined', 'Hello'),
(59, 'masudur', 'rahman', '4386543', 'rana@ussl.com', 0, 'rana123', '', '', '', '', '', '', 'i want to join'),
(60, 'masudur', 'rahman', '4386543', 'rana@ussl.com', 1, 'rana123', '', '', 'bangladesh', 'dhaka', 'dhaka', '', 'i want to join');

-- --------------------------------------------------------

--
-- Table structure for table `pageprivilage`
--

CREATE TABLE IF NOT EXISTS `pageprivilage` (
  `id` int(3) NOT NULL,
  `pageName` varchar(50) NOT NULL,
  `pageGroup` varchar(20) NOT NULL,
  `pageTitle` varchar(50) NOT NULL,
  `adminGroup` tinyint(1) NOT NULL,
  `managerGroup` tinyint(1) NOT NULL,
  `reportGroup` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pageprivilage`
--

INSERT INTO `pageprivilage` (`id`, `pageName`, `pageGroup`, `pageTitle`, `adminGroup`, `managerGroup`, `reportGroup`, `created`) VALUES
(1, 'master.php', 'headmenu', 'Configuration', 1, 0, 0, '0000-00-00 00:00:00'),
(2, 'indexPurchase.php', 'headmenu', 'Purchase', 1, 1, 0, '2013-02-26 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `productinfo`
--

CREATE TABLE IF NOT EXISTS `productinfo` (
  `itemId` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `category` int(11) NOT NULL,
  `supplierId` decimal(8,2) NOT NULL,
  `description` varchar(150) NOT NULL,
  `costPrice` int(4) NOT NULL COMMENT 'purchase price',
  `vat` int(5) NOT NULL,
  `quantity` int(11) NOT NULL COMMENT 'stock',
  `openingStock` int(5) NOT NULL DEFAULT '0',
  `unitPrice` decimal(11,2) NOT NULL COMMENT 'sale price',
  `recordLebel` varchar(20) NOT NULL,
  `location` varchar(15) NOT NULL,
  `allowAltDescription` varchar(100) NOT NULL,
  `featureadd` int(11) NOT NULL DEFAULT '0',
  `created` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productinfo`
--

INSERT INTO `productinfo` (`itemId`, `name`, `category`, `supplierId`, `description`, `costPrice`, `vat`, `quantity`, `openingStock`, `unitPrice`, `recordLebel`, `location`, `allowAltDescription`, `featureadd`, `created`) VALUES
(1, 'Samsung Mobile', 4, '3.00', 'this a high configaretion mobile', 7500, 0, 25, 2, '8500.00', 'Samsung Mobile', 'Estern Plaza', '', 0, '2012-11-26'),
(2, 'Android T-Shirt', 2, '2.00', 'This is Android T-Shirt', 600, 0, 13, 0, '500.00', 'Android T-Shirt', 'Dhanmondi Branc', '', 1, '2012-11-26'),
(3, 'Google T-Shirt', 2, '2.00', 'This is Google T-Shirt', 400, 0, 10, 0, '580.00', 'Ladis T-Shirt', 'Dhanmondi Branc', '', 1, '2012-11-26'),
(4, 'YouTube T-Shirt', 1, '3.00', 'This is YouTube T-Shirt', 350, 0, 10, 0, '450.00', 'YouTube T-Shirt', 'New Market Bran', '', 1, '2012-11-26'),
(5, 'Google Cap', 1, '2.00', 'This is Google Cap', 180, 0, 8, 0, '250.00', 'Google Cap', 'Dhanmondi Branc', '', 0, '2012-11-26'),
(6, 'Facebook T-Shirt', 1, '2.00', 'This is a Facebook T-Shirt', 200, 0, 10, 0, '250.00', 'KK T-Shirt', 'Dhanmondi Branc', '', 0, '2012-11-26'),
(7, 'WebCam', 5, '3.00', 'A4Tech WebCam', 1400, 0, 3, 0, '1600.00', 'A4Tech WebCam', 'IDB-BCS Com', '', 1, '2012-11-26'),
(8, 'Google Ball', 12, '4.00', 'Google Ball', 180, 0, 8, 0, '250.00', 'Google Ball', 'Dhanmondi Branc', '', 0, '2012-11-26'),
(9, 'Google T-Shirt', 1, '4.00', 'Google Full Tshirt', 180, 0, 8, 0, '250.00', 'Google Tshirt', 'Dhanmondi Branc', '', 0, '2012-11-26'),
(10, 'YouTube T-Shirt', 1, '3.00', 'YouTube T-Shirt', 350, 0, 10, 0, '450.00', 'YouTube T-Shirt', 'Dhaka', '', 0, '2012-12-23'),
(11, 'T-Shirt', 1, '2.00', 'Android T-Shirt', 180, 0, 10, 0, '250.00', 'KK T-Shirt', 'Dhanmondi Branc', '', 0, '2012-12-25'),
(12, 'Asus Laptop', 5, '1.00', 'Asus Laptop', 45000, 0, 3, 0, '50000.00', 'Corei3', 'BCS Computer Ci', '', 1, '2012-12-27'),
(13, 'Sony TV', 3, '12.00', 'Sony Bravia TV', 15000, 0, 3, 0, '18000.00', 'Bravia', 'Dhanmondi', '', 1, '2012-12-27'),
(14, 'HP Laptop', 5, '1.00', 'HP Pavilion DV6 Series', 45000, 0, 3, 0, '50000.00', 'HP Pavilion DV6', 'BCS Computer Ci', '', 1, '2012-12-27'),
(15, 'Kyspersky Anti Virus', 5, '1.00', 'Kyspersky Anti Virus', 600, 0, 3, 0, '800.00', 'Kyspersky 2013', 'BCS Computer Ci', '', 1, '2012-12-27'),
(16, 'Sony TV', 3, '12.00', 'Sony Bravia TV', 15000, 0, 3, 0, '18000.00', 'Bravia', 'Dhanmondi', '', 1, '2012-12-27'),
(17, 'Fuji Camera', 3, '12.00', 'Fuji Camera', 18000, 0, 3, 0, '22000.00', 'Fuji Camera', 'Dhanmondi', '', 0, '2012-12-27'),
(18, 'Cocacola Can', 13, '9.00', 'Cocacola Can', 35, 0, 12, 0, '45.00', 'Cocacola Can', 'Dhanondi', '', 1, '2012-12-27'),
(19, 'Pepsi Can', 13, '9.00', 'Pepsi Can', 30, 0, 15, 0, '40.00', 'Pepsi Can', 'Dhanmondi', '', 1, '2012-12-27'),
(20, '7up Can', 13, '9.00', '7up Can', 25, 0, 10, 0, '35.00', '7up Can', 'Dhanmondi', '', 1, '2012-12-27'),
(21, 'Walton Refrigerator', 3, '12.00', 'Walton Refrigerator', 22000, 0, 5, 0, '28000.00', 'Walton Refrigerator', 'Dhanmondi', '', 1, '2012-12-27'),
(22, 'Mouse', 5, '1.00', 'A4tech Mous', 450, 0, -2, 0, '550.00', 'A4tech Mous', 'BCS ComputerCit', '', 1, '2012-12-27'),
(23, 'iPhone 5', 4, '6.00', 'iPhone 5', 60000, 0, 1, 0, '65000.00', 'iPhone 5', 'BCS ComputerCit', '', 1, '2012-12-27'),
(24, 'iPod 5', 4, '6.00', 'iPod 5', 25000, 0, 8, 0, '30000.00', 'iPod 5', 'BCS ComputerCit', '', 1, '2012-12-27'),
(25, 'Latest Tops', 2, '5.00', 'Latest Tops', 800, 0, 10, 0, '1000.00', 'Salower', 'Dhanmondi', '', 1, '2012-12-27'),
(26, 'Salower', 2, '5.00', 'Salower', 900, 0, 5, 0, '1200.00', 'Salower', 'Dhanmondi', '', 1, '2012-12-27'),
(27, 'Mens Jacket', 1, '2.00', 'Mens Jacket', 1500, 0, 12, 0, '1800.00', 'Jacket', 'Dhanmondi', '', 1, '2012-12-28'),
(28, 'Monitor DELL', 5, '1.00', 'Monitor DELL', 10000, 0, 5, 0, '12000.00', 'DELL LCD Monitor', 'BCS Computercit', '', 1, '2013-01-07'),
(29, 'Biscuite', 10, '8.00', 'Vitiamin Biscuit', 250, 0, 12, 0, '300.00', 'Vitiamin Biscuit', 'Dhanmondi', '', 0, '0000-00-00'),
(30, 'Biscuite', 10, '8.00', 'Chocolate Biscuit', 150, 0, 9, 0, '175.00', 'Chocolate Biscuit', 'Dhanmondi', '', 1, '0000-00-00'),
(31, 'T-Shirt', 1, '2.00', 'Barseluna T-Shirt', 550, 0, 10, 0, '650.00', 'Barseluna', 'Dhanmondi', '', 0, '0000-00-00'),
(32, 'T-Shirt Brazil', 1, '2.00', 'T-Shirt Brazil', 450, 0, 10, 0, '550.00', 'Brazil T-Shirt', 'Dhanmondi', '', 0, '0000-00-00'),
(33, 'Gameing PC', 5, '1.00', 'Hi Lavel Gaming PC', 150000, 0, 3, 0, '180000.00', 'Hi Lavel Gaming PC', 'BCS Computercit', '', 1, '0000-00-00'),
(34, 'Toyota Car', 3, '12.00', 'Latest Toyota Car', 4500000, 0, 2, 0, '5000000.00', 'Toyota Car', 'Kakrial Showroo', '', 0, '0000-00-00'),
(35, 'Cup', 9, '4.00', 'Biscuit & Cup', 140, 0, 5, 0, '160.00', 'Biscuit & Cup', 'Dhanmondi', '', 0, '0000-00-00'),
(36, 'iPhone 5', 4, '6.00', 'iPhone 5', 48000, 0, 4, 0, '60000.00', 'iPhone 5', 'Estern Plaza', '', 0, '0000-00-00'),
(37, 'Chocolate', 6, '7.00', 'Chocolate', 120, 0, 10, 0, '150.00', 'Chocolate', 'Dhanmondi', '', 0, '0000-00-00'),
(38, 'i Phone 5', 4, '0.00', 'i Phone 5', 55000, 0, 5, 0, '65000.00', 'i Phone 5', 'Estern Plaza', '', 1, '0000-00-00'),
(39, 'i Phone 4GS', 4, '6.00', 'i Phone 4GS', 45000, 0, 5, 0, '50000.00', 'i Phone 4GS', 'Estern Plaza', '', 0, '0000-00-00'),
(40, 'Birthday Cake', 8, '3.00', 'Birthday Cake', 500, 0, 4, 0, '650.00', 'Birthday Cake 2p', 'Dhanmondi', '', 0, '0000-00-00'),
(41, 'Birthday Cake', 8, '3.00', 'Birthday Cake 2p', 450, 0, 5, 0, '550.00', 'Birthday Cake', 'Dhanmondi', '', 0, '0000-00-00'),
(42, 'T-Shart YouTube', 2, '5.00', 'T-Shart YouTube', 180, 0, 10, 0, '250.00', 'T-Shart YouTube', 'Dhanmondi', '', 1, '0000-00-00'),
(43, 'Food', 9, '3.00', 'Food', 55, 0, 7, 2, '85.00', 'Food', 'Dhanmondi', '', 0, '0000-00-00'),
(44, 'Nokia N8', 4, '6.00', 'Nokia N8', 40000, 0, 4, 0, '45000.00', 'Nokia N8 8GB', 'BCS Computercit', '', 0, '0000-00-00'),
(45, 'Fair&Lovely', 7, '10.00', 'Fair&Lovely', 150, 0, 5, 0, '175.00', 'Lovely', 'Dhanmondi', '', 0, '2013-01-07'),
(46, 'DVD Windows7', 5, '1.00', 'DVD Disk Windows7', 60, 0, 12, 0, '80.00', 'Windows7 Ultimate', 'BCS Computercit', '', 1, '0000-00-00'),
(47, 'PenDrive', 5, '1.00', 'PenDrive', 800, 0, 5, 0, '1000.00', 'Burgar PenDrive', 'BCS Computercit', '', 0, '2013-01-07'),
(48, 'Jack Fruite', 9, '4.00', 'Uncommon Fruite', 80, 0, 5, 0, '120.00', 'Jack Fruite', 'Dhanmondi', '', 0, '2013-01-07'),
(49, 'Jack Fruite', 9, '4.00', 'Jack Fruite', 80, 0, 5, 0, '100.00', 'Jack Fruite', 'Dhanmondi', '', 0, '2013-01-07'),
(50, 'Milkvita', 11, '11.00', 'Milkvita', 80, 0, 5, 0, '100.00', 'Milkvita', 'Dhanmondi', '', 0, '2013-01-07'),
(51, 'Milkvita Big', 11, '11.00', 'Milkvita Big', 150, 0, 62, 50, '175.00', 'Milkvita Big', 'Dhanmondi', '', 0, '2013-01-07'),
(52, 'Bike', 3, '12.00', 'Latest Bike', 180000, 0, 2, 0, '250000.00', 'Bike Latest', 'Dhanmondi', '', 1, '2013-01-07'),
(53, 'Parse', 7, '5.00', 'Parse', 400, 0, 5, 0, '500.00', 'Parse', 'Dhanmondi', '', 0, '2013-01-07'),
(55, 'Windows Server 2008', 14, '13.00', 'Networking Book', 800, 0, 10, 0, '1200.00', 'Windows Server 2008', 'Dhanmondi', '', 1, '2013-02-25'),
(56, 'Sweets', 15, '13.00', 'Diabaties', 140, 0, 10, 0, '200.00', 'Sweet', 'Dhanmondi', '', 1, '2013-02-25'),
(57, 'toyota', 16, '1.00', 'nnn', 400000, 0, 2, 0, '500000.00', '1', 'a1', '', 1, '2013-03-25'),
(58, 'kids set( 2 piece nit)', 17, '1.00', 'new kkids item', 400, 5, 35, 0, '700.00', '', 'A5', '', 1, '2013-06-09'),
(59, 'pajama', 1, '1.00', 'new pajama for men', 250, 5, 15, 0, '400.00', '', 'A5', '', 1, '2013-06-24');

-- --------------------------------------------------------

--
-- Table structure for table `productrequest`
--

CREATE TABLE IF NOT EXISTS `productrequest` (
  `id` int(5) NOT NULL,
  `request` text NOT NULL,
  `customerID` int(5) NOT NULL,
  `userid` int(3) NOT NULL,
  `requestdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='customer''s product request table';

-- --------------------------------------------------------

--
-- Table structure for table `productreviews`
--

CREATE TABLE IF NOT EXISTS `productreviews` (
  `id` int(7) NOT NULL,
  `itemId` int(11) NOT NULL,
  `customername` varchar(30) NOT NULL,
  `customerid` int(10) NOT NULL,
  `customerreview` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `postdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productreviews`
--

INSERT INTO `productreviews` (`id`, `itemId`, `customername`, `customerid`, `customerreview`, `rating`, `status`, `postdate`) VALUES
(1, 26, 'mamun', 58, 'some comments have been made', 4, 0, '2013-03-08 13:12:21'),
(2, 26, 'abu', 58, 'here is some comment', 2, 0, '2013-03-08 13:14:14'),
(3, 17, 'papps', 0, 'this is a test review', 5, 0, '2013-03-09 02:22:11'),
(4, 17, 'papps', 58, 'this is a new review', 5, 0, '2013-03-09 02:26:36'),
(5, 14, 'mamun', 0, 'first review', 3, 0, '2013-03-09 21:59:10'),
(6, 14, 'mamun', 0, 'second review', 5, 0, '2013-03-10 02:36:21'),
(7, 14, 'Google', 0, 'The server at www.google.com can&#039;t be found, because the DNS lookup failed. DNS is the network service that translates a website&#039;s name to its Internet address. This error is most often caused by having no connection to the Internet or a misconfigured network. It can also be caused by an unresponsive DNS server or a firewall preventing Google Chrome from accessing the network.', 5, 0, '2013-03-10 02:38:38'),
(8, 17, 'emu', 0, 'good camera for amateur like me', 3, 0, '2013-03-10 10:15:15'),
(9, 17, 'mamun', 0, 'very good camera', 4, 0, '2013-03-24 16:11:44');

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchaseId` int(10) NOT NULL,
  `adminId` int(10) NOT NULL,
  `purchaseTime` int(11) NOT NULL,
  `purchaserId` int(10) NOT NULL,
  `paymentType` int(5) NOT NULL,
  `comments` varchar(100) NOT NULL,
  `grandTotal` int(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`purchaseId`, `adminId`, `purchaseTime`, `purchaserId`, `paymentType`, `comments`, `grandTotal`) VALUES
(1, 1, 22, 1, 5, 'taka baki ase', 2900),
(2, 0, 22, 0, 0, '', 40500),
(3, 1, 22, 2, 2, 'uiytuy', 37500);

-- --------------------------------------------------------

--
-- Table structure for table `purchaseitem`
--

CREATE TABLE IF NOT EXISTS `purchaseitem` (
  `id` int(11) NOT NULL,
  `purchaseId` int(10) NOT NULL,
  `purchaserId` int(10) NOT NULL,
  `itemId` int(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  `productSerialNumber` int(10) NOT NULL,
  `qtyPurchased` int(10) NOT NULL,
  `itemUnitPrice` int(10) NOT NULL,
  `itemCostPrice` int(10) NOT NULL,
  `discountPercent` int(10) NOT NULL,
  `vat` int(10) NOT NULL,
  `totalPrice` int(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchaseitem`
--

INSERT INTO `purchaseitem` (`id`, `purchaseId`, `purchaserId`, `itemId`, `description`, `productSerialNumber`, `qtyPurchased`, `itemUnitPrice`, `itemCostPrice`, `discountPercent`, `vat`, `totalPrice`) VALUES
(1, 1, 1, 3, 'This is Google T-Shirt', 0, 5, 400, 2000, 0, 0, 2000),
(2, 1, 1, 5, 'This is Google Cap', 0, 5, 180, 900, 0, 0, 900),
(3, 2, 0, 1, 'this a high configaretion mobile', 0, 5, 7500, 37500, 0, 0, 37500),
(4, 2, 0, 2, 'This is Android T-Shirt', 0, 5, 600, 3000, 0, 0, 3000),
(5, 3, 2, 1, 'this a high configaretion mobile', 0, 5, 7500, 37500, 0, 0, 37500);

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE IF NOT EXISTS `sale` (
  `saleId` int(11) NOT NULL,
  `saleTime` datetime NOT NULL,
  `customerId` varchar(11) NOT NULL,
  `employeeId` varchar(11) NOT NULL,
  `paymentType` varchar(30) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `grandTotal` decimal(15,2) NOT NULL,
  `itemsDelivered` varchar(25) NOT NULL DEFAULT 'Undelivered',
  `amountReceived` varchar(20) NOT NULL DEFAULT 'Unpaid'
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sale`
--

INSERT INTO `sale` (`saleId`, `saleTime`, `customerId`, `employeeId`, `paymentType`, `comment`, `grandTotal`, `itemsDelivered`, `amountReceived`) VALUES
(1, '2009-01-01 02:28:10', '25', '12', '2', 'hsdgkhkshgkhsk', '569.00', 'Undelivered', 'Unpaid'),
(2, '2009-01-09 03:01:01', '15', '12', '1', 'The Cheque is valid for sonali bank', '1330.00', 'Undelivered', 'Unpaid'),
(3, '2009-01-01 13:23:53', '12', '15', 'online', 'the cap is', '748.00', 'Undelivered', 'Unpaid'),
(4, '2009-01-02 14:33:26', '7', '09876', 'online', 'there is no comment', '787.50', 'Undelivered', 'Unpaid'),
(5, '2009-01-01 14:43:22', '4', '01723454321', 'online', 'Bangladesh is a ismallcountry ', '1396.50', 'Delivered', 'Unpaid'),
(6, '2009-01-01 17:27:35', '7', '09876', 'online', 'now i paid 50% another amount after 15 days ', '1312.50', 'Undelivered', 'Unpaid'),
(7, '2009-01-01 14:16:11', '7', '09876', 'online', 'Bangladesh is a small country ', '3622.50', 'Undelivered', 'Unpaid'),
(8, '2009-01-01 14:17:55', '7', '09876', 'online', '', '204750.00', 'Undelivered', 'Unpaid'),
(9, '2009-01-01 14:23:24', '7', '09876', 'online', '', '1050.00', 'Undelivered', 'Unpaid'),
(10, '2009-01-01 14:24:11', '7', '09876', 'online', '', '262.50', 'Undelivered', 'Unpaid'),
(11, '2009-01-01 14:24:41', '7', '09876', 'online', '', '52500.00', 'Undelivered', 'Unpaid'),
(12, '2009-01-01 14:30:10', '7', '09876', 'online', '', '8925.00', 'Undelivered', 'Unpaid'),
(13, '2009-01-01 14:30:41', '7', '09876', 'online', '', '52500.00', 'Undelivered', 'Unpaid'),
(14, '2009-01-01 14:32:36', '7', '09876', 'online', '', '8925.00', 'Undelivered', 'Unpaid'),
(28, '2013-02-22 16:28:59', '1', '01912604728', 'online', '', '36.75', 'Undelivered', 'Unpaid'),
(27, '2013-02-22 16:28:52', '1', '01912604728', 'online', '', '183.75', 'Undelivered', 'Unpaid'),
(26, '2013-02-22 16:28:46', '1', '01912604728', 'online', '', '52500.00', 'Undelivered', 'Unpaid'),
(25, '2013-02-22 16:28:39', '1', '01912604728', 'online', '', '157.50', 'Undelivered', 'Unpaid'),
(24, '2013-02-22 16:28:19', '1', '01912604728', 'online', '', '262.50', 'Undelivered', 'Unpaid'),
(23, '2013-02-22 16:28:02', '1', '01912604728', 'online', '', '1260.00', 'Undelivered', 'Unpaid'),
(22, '2013-02-22 16:27:37', '1', '01912604728', 'online', '', '2131.50', 'Undelivered', 'Unpaid'),
(29, '2013-02-22 16:29:05', '1', '01912604728', 'online', '', '157.50', 'Undelivered', 'Unpaid'),
(30, '2013-02-22 16:30:12', '1', '01912604728', 'online', '', '3360.00', 'Undelivered', 'Unpaid'),
(31, '2013-02-22 16:31:06', '39', '01670462007', 'online', '', '4053.00', 'Undelivered', 'Unpaid'),
(32, '2013-02-22 16:37:02', '39', '01670462007', 'online', '', '2310.00', 'Undelivered', 'Unpaid'),
(33, '2013-02-22 16:37:09', '1', '01912604728', 'online', '', '73080.00', 'Undelivered', 'Unpaid'),
(34, '2022-02-13 04:37:37', '', '', '0', '', '0.00', 'Undelivered', 'Unpaid'),
(35, '2022-02-13 04:38:46', '', '', '0', '', '12300.00', 'Undelivered', 'Unpaid'),
(36, '2022-02-13 06:59:33', '', '', '0', '', '43500.00', 'Undelivered', 'Unpaid'),
(37, '2022-02-13 07:05:20', '', '', '0', '', '43500.00', 'Undelivered', 'Unpaid'),
(38, '2022-02-13 07:08:05', '1', '1', '0', 'sdf', '43500.00', 'Undelivered', 'Unpaid'),
(39, '2013-02-23 12:38:44', '4', '01911561554', 'online', '', '54652.50', 'Undelivered', 'Unpaid'),
(40, '2013-02-24 01:57:17', '4', '01911561554', 'online', '', '27856.50', 'Undelivered', 'Unpaid'),
(41, '2013-02-25 16:31:07', '4', '01911561554', 'online', '', '94.50', 'Undelivered', 'Unpaid'),
(42, '2025-02-13 06:26:34', '1', '1', '0', 'long list sell', '264260.00', 'Undelivered', 'Unpaid'),
(43, '2013-03-06 18:51:16', '58', '01911039525', 'online', '', '58590.00', 'Delivered', 'Paid'),
(44, '2013-03-09 15:39:26', '58', '01911039525', 'online', '', '52500.00', 'Undelivered ', 'Unpaid'),
(45, '2013-03-09 15:42:48', '58', '01911039525', 'online', '', '99999999.99', 'Undelivered ', 'Unpaid'),
(46, '2013-03-10 11:01:13', '', '', 'online', '', '71400.00', 'Undelivered', 'Unpaid'),
(47, '2013-03-10 11:01:40', '', '', 'online', '', '54180.00', 'Undelivered', 'Unpaid'),
(48, '2013-03-10 15:37:03', '', '', 'online', '', '52500.00', 'Undelivered', 'Unpaid'),
(49, '2013-03-10 16:13:32', '58', '01911039525', 'online', '', '22260.00', 'Delivered', 'Paid'),
(50, '2013-03-10 04:22:46', '', '', '0', '', '390000.00', 'Undelivered', 'Unpaid'),
(51, '2013-03-10 23:18:15', '58', '01911039525', 'online', '', '3412.50', 'Delivered', 'Paid'),
(52, '2013-03-10 11:20:14', '', '', '0', '', '61175.00', 'Undelivered', 'Unpaid'),
(53, '2013-03-14 04:46:46', '58', '01911039525', 'online', '', '166800.00', 'Undelivered', 'Unpaid'),
(54, '2013-03-14 04:49:35', '', '', '0', '', '267100.00', 'Undelivered', 'Unpaid'),
(55, '2013-03-25 05:14:08', '58', '01911039525', 'online', '', '232700.00', 'Delivered', 'Paid'),
(56, '2013-03-25 05:23:23', '4', '2', '0', '', '66000.00', 'Undelivered', 'Unpaid'),
(57, '2013-06-09 11:56:13', '', '', '0', '', '48500.00', 'Undelivered', 'Unpaid'),
(58, '2013-06-09 11:59:58', '', '', '4', '', '7000.00', 'Undelivered', 'Unpaid');

-- --------------------------------------------------------

--
-- Table structure for table `saleitem`
--

CREATE TABLE IF NOT EXISTS `saleitem` (
  `id` int(11) NOT NULL,
  `saleId` varchar(11) NOT NULL,
  `itemId` varchar(11) NOT NULL,
  `itemName` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `productSerialNumber` varchar(20) NOT NULL,
  `qtyPurchased` int(11) NOT NULL,
  `itemUnitPrice` decimal(10,2) NOT NULL,
  `itemCostPrice` decimal(10,2) NOT NULL,
  `discountPercent` int(11) NOT NULL,
  `vat` decimal(10,2) NOT NULL,
  `totalPrice` decimal(14,2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `saleitem`
--

INSERT INTO `saleitem` (`id`, `saleId`, `itemId`, `itemName`, `description`, `productSerialNumber`, `qtyPurchased`, `itemUnitPrice`, `itemCostPrice`, `discountPercent`, `vat`, `totalPrice`) VALUES
(1, '1', '23', '', 'there is no comment', '', 2, '250.00', '500.00', 5, '15.00', '569.00'),
(2, '2', '23', '', 'there is no comment', '', 2, '250.00', '500.00', 5, '0.00', '475.00'),
(3, '2', '25', '', '0', '', 2, '450.00', '900.00', 5, '0.00', '855.00'),
(4, '3', '22', '', 'there is no comment', '', 3, '250.00', '750.00', 5, '5.00', '748.00'),
(5, '4', '23', '', 'there is no comment', '', 2, '250.00', '500.00', 0, '25.00', '525.00'),
(6, '4', '22', '', '0', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(7, '5', '15', '', 'Good quality', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(8, '5', '18', '', '0', '', 1, '580.00', '580.00', 0, '29.00', '609.00'),
(9, '5', '19', '', '0', '', 1, '500.00', '500.00', 0, '25.00', '525.00'),
(10, '6', '15', '', 'Good quality', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(11, '6', '22', '', '0', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(12, '6', '23', '', 'there is no comment', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(13, '6', '19', '', '0', '', 1, '500.00', '500.00', 0, '25.00', '525.00'),
(14, '7', '21', '', '0', '', 2, '1600.00', '3200.00', 0, '160.00', '3360.00'),
(15, '7', '22', '', '0', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(16, '8', '39', '', '0', '', 3, '65000.00', '195000.00', 0, '9750.00', '204750.00'),
(17, '9', '19', '', '0', '', 2, '500.00', '1000.00', 0, '50.00', '1050.00'),
(18, '10', '15', '', 'Good quality', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(19, '11', '27', '', '0', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(20, '12', '20', '', 'there is no comment', '', 1, '8500.00', '8500.00', 0, '425.00', '8925.00'),
(21, '13', '27', '', '0', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(22, '14', '20', '', 'there is no comment', '', 1, '8500.00', '8500.00', 0, '425.00', '8925.00'),
(23, '15', '19', '', '0', '', 2, '500.00', '1000.00', 0, '50.00', '1050.00'),
(24, '16', '17', '', 'Fuji Camera', '', 1, '22000.00', '22000.00', 0, '1100.00', '23100.00'),
(25, '16', '18', '', 'Cocacola Can', '', 1, '45.00', '45.00', 0, '2.25', '47.25'),
(26, '16', '16', '', 'Sony Bravia TV', '', 1, '18000.00', '18000.00', 0, '900.00', '18900.00'),
(27, '16', '19', '', 'Pepsi Can', '', 1, '40.00', '40.00', 0, '2.00', '42.00'),
(28, '17', '4', '', 'This is YouTube T-Shirt', '', 3, '450.00', '1350.00', 0, '67.50', '1417.50'),
(29, '17', '3', '', 'This is Google T-Shirt', '', 2, '580.00', '1160.00', 0, '58.00', '1218.00'),
(30, '18', '50', '', 'Milkvita', '', 1, '100.00', '100.00', 0, '5.00', '105.00'),
(42, '23', '40', '', 'Birthday Cake', '', 1, '650.00', '650.00', 0, '32.50', '682.50'),
(41, '23', '41', '', 'Birthday Cake 2p', '', 1, '550.00', '550.00', 0, '27.50', '577.50'),
(40, '22', '2', '', 'This is Android T-Shirt', '', 2, '500.00', '1000.00', 0, '50.00', '1050.00'),
(39, '22', '3', '', 'This is Google T-Shirt', '', 1, '580.00', '580.00', 0, '29.00', '609.00'),
(38, '22', '4', '', 'This is YouTube T-Shirt', '', 1, '450.00', '450.00', 0, '22.50', '472.50'),
(43, '24', '8', '', 'Google Ball', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(44, '25', '37', '', 'Chocolate', '', 1, '150.00', '150.00', 0, '7.50', '157.50'),
(45, '26', '12', '', 'Asus Laptop', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(46, '27', '51', '', 'Milkvita Big', '', 1, '175.00', '175.00', 0, '8.75', '183.75'),
(47, '28', '20', '', '7up Can', '', 1, '35.00', '35.00', 0, '1.75', '36.75'),
(48, '29', '37', '', 'Chocolate', '', 1, '150.00', '150.00', 0, '7.50', '157.50'),
(49, '30', '4', '', 'This is YouTube T-Shirt', '', 1, '450.00', '450.00', 0, '22.50', '472.50'),
(50, '30', '9', '', 'Google Full Tshirt', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(51, '30', '10', '', 'YouTube T-Shirt', '', 1, '450.00', '450.00', 0, '22.50', '472.50'),
(52, '30', '11', '', 'Android T-Shirt', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(53, '30', '27', '', 'Mens Jacket', '', 1, '1800.00', '1800.00', 0, '90.00', '1890.00'),
(54, '31', '4', '', 'This is YouTube T-Shirt', '', 6, '450.00', '2700.00', 0, '135.00', '2835.00'),
(55, '31', '3', '', 'This is Google T-Shirt', '', 2, '580.00', '1160.00', 0, '58.00', '1218.00'),
(56, '32', '4', '', 'This is YouTube T-Shirt', '', 1, '450.00', '450.00', 0, '22.50', '472.50'),
(57, '32', '5', '', 'This is Google Cap', '', 7, '250.00', '1750.00', 0, '87.50', '1837.50'),
(58, '33', '12', '', 'Asus Laptop', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(59, '33', '7', '', 'A4Tech WebCam', '', 1, '1600.00', '1600.00', 0, '80.00', '1680.00'),
(60, '33', '13', '', 'Sony Bravia TV', '', 1, '18000.00', '18000.00', 0, '900.00', '18900.00'),
(61, '34', '2', 'Android T-Shirt', 'This is Android T-Shirt', '', 0, '500.00', '0.00', 0, '0.00', '0.00'),
(62, '35', '2', 'Android T-Shirt', 'This is Android T-Shirt', '', 2, '500.00', '1000.00', 75, '0.00', '925.00'),
(63, '35', '3', 'Google T-Shirt', 'This is Google T-Shirt', '', 5, '580.00', '2900.00', 0, '0.00', '2900.00'),
(64, '35', '5', 'Google Cap', 'This is Google Cap', '', 6, '250.00', '1500.00', 0, '0.00', '1500.00'),
(65, '35', '5', 'Google Cap', 'This is Google Cap', '', 4, '250.00', '1000.00', 0, '0.00', '1000.00'),
(66, '35', '6', 'Facebook T-Shirt', 'This is a Facebook T-Shirt', '', 7, '250.00', '1750.00', 0, '0.00', '1750.00'),
(67, '35', '7', 'WebCam', 'A4Tech WebCam', '', 1, '1600.00', '1600.00', 0, '0.00', '1600.00'),
(68, '35', '9', 'Google T-Shirt', 'Google Full Tshirt', '', 1, '250.00', '250.00', 0, '0.00', '250.00'),
(69, '35', '22', 'Mouse', 'A4tech Mous', '', 1, '550.00', '550.00', 0, '0.00', '550.00'),
(70, '35', '25', 'Latest Tops', 'Latest Tops', '', 1, '1000.00', '1000.00', 0, '0.00', '1000.00'),
(71, '35', '30', 'Biscuite', 'Chocolate Biscuit', '', 1, '175.00', '175.00', 0, '0.00', '175.00'),
(72, '35', '31', 'T-Shirt', 'Barseluna T-Shirt', '', 1, '650.00', '650.00', 0, '0.00', '650.00'),
(73, '37', '1', 'Samsung Mobile', 'this a high configaretion mobile', '', 5, '8500.00', '42500.00', 0, '0.00', '42500.00'),
(74, '37', '2', 'Android T-Shirt', 'This is Android T-Shirt', '', 2, '500.00', '1000.00', 0, '0.00', '1000.00'),
(75, '38', '1', 'Samsung Mobile', 'this a high configaretion mobile', '', 5, '8500.00', '42500.00', 0, '0.00', '42500.00'),
(76, '38', '2', 'Android T-Shirt', 'This is Android T-Shirt', '', 2, '500.00', '1000.00', 0, '0.00', '1000.00'),
(77, '39', '14', '', 'HP Pavilion DV6 Series', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(78, '39', '15', '', 'Kyspersky Anti Virus', '', 2, '800.00', '1600.00', 0, '80.00', '1680.00'),
(79, '39', '4', '', 'This is YouTube T-Shirt', '', 1, '450.00', '450.00', 0, '22.50', '472.50'),
(80, '40', '4', '', 'This is YouTube T-Shirt', '', 2, '450.00', '900.00', 0, '45.00', '945.00'),
(81, '40', '5', '', 'This is Google Cap', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(82, '40', '6', '', 'This is a Facebook T-Shirt', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(83, '40', '9', '', 'Google Full Tshirt', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(84, '40', '10', '', 'YouTube T-Shirt', '', 1, '450.00', '450.00', 0, '22.50', '472.50'),
(85, '40', '11', '', 'Android T-Shirt', '', 1, '250.00', '250.00', 0, '12.50', '262.50'),
(86, '40', '7', '', 'A4Tech WebCam', '', 1, '1600.00', '1600.00', 0, '80.00', '1680.00'),
(87, '40', '3', '', 'This is Google T-Shirt', '', 1, '580.00', '580.00', 0, '29.00', '609.00'),
(88, '40', '17', '', 'Fuji Camera', '', 1, '22000.00', '22000.00', 0, '1100.00', '23100.00'),
(89, '41', '18', '', 'Cocacola Can', '', 2, '45.00', '90.00', 0, '4.50', '94.50'),
(90, '42', '1', 'Samsung Mobile', 'this a high configaretion mobile', '', 2, '8500.00', '17000.00', 0, '0.00', '17000.00'),
(91, '42', '2', 'Android T-Shirt', 'This is Android T-Shirt', '', 2, '500.00', '1000.00', 0, '0.00', '1000.00'),
(92, '42', '3', 'Google T-Shirt', 'This is Google T-Shirt', '', 2, '580.00', '1160.00', 0, '0.00', '1160.00'),
(93, '42', '4', 'YouTube T-Shirt', 'This is YouTube T-Shirt', '', 2, '450.00', '900.00', 0, '0.00', '900.00'),
(94, '42', '5', 'Google Cap', 'This is Google Cap', '', 2, '250.00', '500.00', 0, '0.00', '500.00'),
(95, '42', '6', 'Facebook T-Shirt', 'This is a Facebook T-Shirt', '', 2, '250.00', '500.00', 0, '0.00', '500.00'),
(96, '42', '7', 'WebCam', 'A4Tech WebCam', '', 2, '1600.00', '3200.00', 0, '0.00', '3200.00'),
(97, '42', '8', 'Google Ball', 'Google Ball', '', 2, '250.00', '500.00', 0, '0.00', '500.00'),
(98, '42', '9', 'Google T-Shirt', 'Google Full Tshirt', '', 2, '250.00', '500.00', 0, '0.00', '500.00'),
(99, '42', '10', 'YouTube T-Shirt', 'YouTube T-Shirt', '', 2, '450.00', '900.00', 0, '0.00', '900.00'),
(100, '42', '11', 'T-Shirt', 'Android T-Shirt', '', 2, '250.00', '500.00', 0, '0.00', '500.00'),
(101, '42', '12', 'Asus Laptop', 'Asus Laptop', '', 2, '50000.00', '100000.00', 0, '0.00', '100000.00'),
(102, '42', '13', 'Sony TV', 'Sony Bravia TV', '', 2, '18000.00', '36000.00', 0, '0.00', '36000.00'),
(103, '42', '14', 'HP Laptop', 'HP Pavilion DV6 Series', '', 2, '50000.00', '100000.00', 0, '0.00', '100000.00'),
(104, '42', '15', 'Kyspersky Anti Virus', 'Kyspersky Anti Virus', '', 2, '800.00', '1600.00', 0, '0.00', '1600.00'),
(105, '43', '3', '', 'This is Google T-Shirt', '', 10, '580.00', '5800.00', 0, '290.00', '6090.00'),
(106, '43', '12', '', 'Asus Laptop', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(107, '44', '39', '', 'i Phone 4GS', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(108, '45', '52', '', 'Latest Bike', '', 1, '250000.00', '250000.00', 0, '12500.00', '262500.00'),
(109, '45', '21', '', 'Walton Refrigerator', '', 1, '28000.00', '28000.00', 0, '1400.00', '29400.00'),
(110, '45', '17', '', 'Fuji Camera', '', 1, '22000.00', '22000.00', 0, '1100.00', '23100.00'),
(111, '45', '23', '', 'iPhone 5', '', 1, '65000.00', '65000.00', 0, '3250.00', '68250.00'),
(112, '45', '16', '', 'Sony Bravia TV', '', 1, '18000.00', '18000.00', 0, '900.00', '18900.00'),
(113, '45', '34', '', 'Latest Toyota Car', '', 200, '5000000.00', '99999999.99', 0, '50000000.00', '99999999.99'),
(114, '46', '12', '', 'Asus Laptop', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(115, '46', '13', '', 'Sony Bravia TV', '', 1, '18000.00', '18000.00', 0, '900.00', '18900.00'),
(116, '47', '7', '', 'A4Tech WebCam', '', 1, '1600.00', '1600.00', 0, '80.00', '1680.00'),
(117, '47', '12', '', 'Asus Laptop', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(118, '48', '12', '', 'Asus Laptop', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(119, '49', '7', '', 'A4Tech WebCam', '', 2, '1600.00', '3200.00', 0, '160.00', '3360.00'),
(120, '49', '13', '', 'Sony Bravia TV', '', 1, '18000.00', '18000.00', 0, '900.00', '18900.00'),
(121, '50', '33', 'Gameing PC', 'Hi Lavel Gaming PC', '', 2, '180000.00', '360000.00', 0, '0.00', '360000.00'),
(122, '50', '24', 'iPod 5', 'iPod 5', '', 1, '30000.00', '30000.00', 0, '0.00', '30000.00'),
(123, '51', '25', '', 'Latest Tops', '', 1, '1000.00', '1000.00', 0, '50.00', '1050.00'),
(124, '51', '26', '', 'Salower', '', 1, '1200.00', '1200.00', 0, '60.00', '1260.00'),
(125, '51', '37', '', 'Chocolate', '', 1, '150.00', '150.00', 0, '7.50', '157.50'),
(126, '51', '29', '', 'Vitiamin Biscuit', '', 3, '300.00', '900.00', 0, '45.00', '945.00'),
(127, '52', '36', 'iPhone 5', 'iPhone 5', '', 1, '60000.00', '60000.00', 0, '0.00', '60000.00'),
(128, '52', '30', 'Biscuite', 'Chocolate Biscuit', '', 3, '175.00', '525.00', 0, '0.00', '525.00'),
(129, '52', '40', 'Birthday Cake', 'Birthday Cake', '', 1, '650.00', '650.00', 0, '0.00', '650.00'),
(130, '53', '7', '', 'A4Tech WebCam', '', 1, '1600.00', '1600.00', 0, '80.00', '1680.00'),
(131, '53', '12', '', 'Asus Laptop', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(132, '53', '14', '', 'HP Pavilion DV6 Series', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(133, '53', '23', '', 'iPhone 5', '', 1, '65000.00', '65000.00', 0, '3250.00', '68250.00'),
(134, '54', '22', 'Mouse', 'A4tech Mous', '', 12, '550.00', '6600.00', 0, '0.00', '6600.00'),
(135, '54', '5', 'Google Cap', 'This is Google Cap', '', 2, '250.00', '500.00', 0, '0.00', '500.00'),
(136, '54', '23', 'iPhone 5', 'iPhone 5', '', 4, '65000.00', '260000.00', 0, '0.00', '260000.00'),
(137, '55', '9', '', 'Google Full Tshirt', '', 10, '250.00', '2500.00', 0, '125.00', '2625.00'),
(138, '55', '14', '', 'HP Pavilion DV6 Series', '', 1, '50000.00', '50000.00', 0, '2500.00', '52500.00'),
(139, '55', '33', '', 'Hi Lavel Gaming PC', '', 1, '180000.00', '180000.00', 0, '9000.00', '189000.00'),
(140, '56', '24', 'iPod 5', 'iPod 5', '', 1, '30000.00', '30000.00', 0, '0.00', '30000.00'),
(141, '56', '16', 'Sony TV', 'Sony Bravia TV', '', 2, '18000.00', '36000.00', 0, '0.00', '36000.00'),
(142, '57', '58', 'kids set( 2 piece nit)', '', '', 5, '700.00', '3500.00', 0, '0.00', '3500.00'),
(143, '57', '44', 'Nokia N8', 'Nokia N8', '', 1, '45000.00', '45000.00', 0, '0.00', '45000.00'),
(144, '58', '58', 'kids set( 2 piece nit)', '', '', 10, '700.00', '7000.00', 0, '0.00', '7000.00');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL,
  `countryId` int(11) NOT NULL,
  `stateName` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `countryId`, `stateName`) VALUES
(1, 1, 'U.P.'),
(2, 1, 'Uttarakhand'),
(3, 2, 'Dhaka'),
(4, 2, 'Chittagong');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `supplier_id` int(10) NOT NULL,
  `first_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `phone_number` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `address_1` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `address_2` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `city` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `state` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `zip_code` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `country` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `comments` varchar(70) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `first_name`, `last_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip_code`, `country`, `comments`) VALUES
(1, 'Md.', 'Shahin', '01911561554', 'dm_shahin@yahoo.com', 'Tallabag, Subahanbag', 'Tery Bazar, Durgapur', 'Susang Durgapur', 'Netrakona', '2420', 'Bangladesh', 'Supplier of Computer and electrical parts some more comments'),
(2, 'Helalur', 'Apon', '01670462007', 'spapon@hotmail.com', 'Azimpur', 'Sirajganj', 'Sirajganj', 'Sirajganj', '1205', 'Bangladesh', 'Supplier of Mens'),
(3, 'Md.', 'Alamin', '01718489837', 'ashikalamin21@yahoo.com', 'Lalbag', 'Barishal', 'Barishal', 'Barishal', '4517', 'Bangladesh', 'Supplier of Food'),
(4, 'Md.', 'Zillur Rahaman', '01912604728', 'zillur@yahoo.com', 'Mirpur', 'Phirujpur', 'Phirujpur', 'Phirujpur', '1546', 'Bangladesh', 'Supplier of Grocery'),
(5, 'Monira', 'Begum', '01710517110', 'monirabgm56@gmail.com', 'Dhanmondi-15', 'Rangpur', 'Rangpur', 'Rangpur', '5486', 'Bangladesh', 'Supplier of Ladies'),
(6, 'Mahmudul', 'Hasan', '01912563302', 'hasan@yahoo.com', 'Jatrabari', 'Comilla', 'Comilla', 'Comilla', '4578', 'Bangladesh', 'Supplier of Mobile'),
(7, 'Tanzil', 'Alam', '01979398706', 'tanzil@yahoo.com', 'Jatrabari', 'Barishal', 'Uzirpur', 'Barishal', '8220', 'Bangladesh', 'Supplier of Chocolate'),
(8, 'Md.', 'Ashraful', '01913993482', 'mailashraf@ymail.com', 'Magbazar', 'Sirsjgonj', 'Khokshabari', 'Sadar', '6700', 'Bangladesh', 'Supplier of Biscuit'),
(9, 'Afzal', 'Hossain', '01716800514', 'afzal_bdkg@yahoo.com', 'Shaymoli', 'Kishoreganj', 'Tarial', 'Kishoreganj', '2316', 'Bnagladesh', 'Supplier of Soft-Drinks'),
(10, 'Md. Basir', 'Rayhan', '01718835880', 'basirrayhan@yahoo.com', 'Mohammadpur', 'Panchagarh', 'Vajanpur', 'Tetulia', '5030', 'Bangladesh', 'Supplier of Cosmetic'),
(11, 'Md.Sefatul', 'Islam', '01712409522', 'sefatuislam@yahoo.com', 'Shahajahanpur', 'Barishal', 'Paisarhat', 'Barishal', '8242', 'Bangladesh', 'Supplier of Baby-Food'),
(12, 'Emdadul', 'Haque', '01713129527', 'emdad1319@gmail.com', 'Shankor', 'Kishorigonj', 'Nikli', 'Barishal', '8242', 'Bangladesh', 'Supplier of Electronics'),
(13, 'Mostofa Kawser', 'Lalin', '01912468140', 'mkhlalin@gmail.com', 'Durgapur', 'Tallahabag', 'Dhaka', 'Dhanmondi', '1208', 'Bangladesh', 'Book Supplier'),
(14, 'ansar', 'ali sarker', '01915164150', 'ansar@gmail.com', 'parbatipur', 'parbatipur', 'parbatipur', 'dinajpur', '1234', 'Bangladesh', 'new supplier');

-- --------------------------------------------------------

--
-- Table structure for table `towninfo`
--

CREATE TABLE IF NOT EXISTS `towninfo` (
  `id` int(11) NOT NULL,
  `townId` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `towninfo`
--

INSERT INTO `towninfo` (`id`, `townId`, `description`) VALUES
(1, 3, 'Pithoragarh is a beautiful town situated in Kumaon region of Uttarakhand. It has an average elevation of 1,514 metres (4,967 feet) above sea level.'),
(2, 2, 'Dehradun also known as Doon is the capital city of Uttarakhand. It is around 250 Kilometers from national capital Delhi.\r\nRice and Lychee are major products of this city.'),
(3, 1, 'Lucknow is the capital city of U.P. or Uttar Pradesh.\r\nLucknow has Asia''s first human DNA bank.\r\nIt is popularly known as The City of Nawabs, Golden City of the East and The Constantinople of India.'),
(4, 6, 'Savar is beside Dhaka.'),
(5, 7, 'IUT is in gazipur');

-- --------------------------------------------------------

--
-- Table structure for table `towns`
--

CREATE TABLE IF NOT EXISTS `towns` (
  `id` int(11) NOT NULL,
  `stateId` int(11) NOT NULL,
  `townName` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `towns`
--

INSERT INTO `towns` (`id`, `stateId`, `townName`) VALUES
(1, 1, 'Lucknow'),
(2, 1, 'Bareilly'),
(3, 2, 'Pithoragarh'),
(4, 2, 'Dehradun'),
(5, 2, 'Nainital'),
(6, 3, 'Savar'),
(7, 3, 'Gazipur'),
(8, 4, 'Lohagara'),
(9, 4, 'Satkania'),
(10, 3, 'Mirpur'),
(11, 3, 'Matijhil'),
(12, 5, 'Pirojpur'),
(13, 5, 'Jhalokati'),
(14, 5, 'Barguna'),
(15, 5, 'Barishal Sadar'),
(16, 5, 'Potuakahali'),
(17, 5, 'Bhola');

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `userID` int(12) NOT NULL,
  `first_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `last_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `phone_number` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `emailaddress_1` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `emailaddress_2` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `city` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `state` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `zip` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `country` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `comments` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `created` date NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`userID`, `first_name`, `last_name`, `phone_number`, `emailaddress_1`, `emailaddress_2`, `city`, `state`, `zip`, `country`, `comments`, `created`) VALUES
(15, 'Tanzil', 'Alom', '0191245987', 'Tasi_satusi@yahoo.com', 'Tasi_satusi@yahoo.co', 'dhaka', 'dhaka', 'dhaka', 'dhaka', 'I m manager of the company', '2013-01-04'),
(16, 'Jilluur', 'Rahman', '0191254678', 'jillur@yahoo.com', 'illur@yahoo.com', 'Dhaka', 'Dhaka', '1205', 'Bangladesh', 'there is no comment', '2009-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'holmes', 'sherlockholmes'),
(2, 'watson', 'johnwatson'),
(3, 'sati', 'pranay'),
(4, 'mantu', 'ajayjoshi'),
(5, 'sahji', 'brijsah'),
(6, 'vijay', 'vijayjoshi'),
(7, 'brij', 'brijsah'),
(8, 'arjun', 'samant'),
(9, 'jyotsna', 'sonawane'),
(12, 'ravindra', 'pokharia'),
(13, 'prakash', 'joshi'),
(14, 'sahji2', 'aloklal'),
(15, 'basant', 'bhandari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`admin_email`), ADD UNIQUE KEY `username` (`admin_id`);

--
-- Indexes for table `categorytable`
--
ALTER TABLE `categorytable`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerID`);

--
-- Indexes for table `pageprivilage`
--
ALTER TABLE `pageprivilage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productinfo`
--
ALTER TABLE `productinfo`
  ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `productrequest`
--
ALTER TABLE `productrequest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productreviews`
--
ALTER TABLE `productreviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`purchaseId`);

--
-- Indexes for table `purchaseitem`
--
ALTER TABLE `purchaseitem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`saleId`);

--
-- Indexes for table `saleitem`
--
ALTER TABLE `saleitem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`), ADD UNIQUE KEY `phone_number` (`phone_number`,`email`);

--
-- Indexes for table `towninfo`
--
ALTER TABLE `towninfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `towns`
--
ALTER TABLE `towns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `categorytable`
--
ALTER TABLE `categorytable`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `pageprivilage`
--
ALTER TABLE `pageprivilage`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `productinfo`
--
ALTER TABLE `productinfo`
  MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `productrequest`
--
ALTER TABLE `productrequest`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `productreviews`
--
ALTER TABLE `productreviews`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `purchaseId` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `purchaseitem`
--
ALTER TABLE `purchaseitem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sale`
--
ALTER TABLE `sale`
  MODIFY `saleId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `saleitem`
--
ALTER TABLE `saleitem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `supplier_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `towninfo`
--
ALTER TABLE `towninfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `towns`
--
ALTER TABLE `towns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `userID` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
