<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="style.css" media="all" />
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="main_container">
			<div class="container">
				<header class="header_area">
					<div class="row">
						<div class="col-xs-12">
							<div class="header">
								<h2>Student</h2>
								<p>IT SCHOLARSHIP</p>
							</div>
						</div>
					</div>
				</header>
				<nav class="navigation">
					<div class="row">
						<div class="col-md-11 col-md-offset-1">
							<div class="nav">
								<ul>
									<li><a href="#">Student</a></li>
									<li><a href="#">Course</a></li>
									<li><a href="#">Batch</a></li>
								</ul>
							</div>
						</div>
					</div>
				</nav>
				<section class="content_area">
					<div class="row">
						<div class="col-md-3 col-md-offset-1">
							<div class="h_menu">
								<ul>
									<li><a href="#home" data-toggle="tab">Student</a></li>
									<li><a href="#about" data-toggle="tab">Course</a></li>
									<li><a href="#content" data-toggle="tab">Batch</a></li>
									<li><a href="#contact" data-toggle="tab">Demo</a></li>
								</ul>
							</div>
							
							<div class="search_area">
								<form action="#">
									<input type="search" name="search" id="search" />
									<input type="button" name="submit" id="submit" value="Go" />
								</form>
							</div>
						</div>
						<div class="col-md-8">
							<div class="content tab-content">
								<div class="tab-pane active" id="home">
									<?php
										include_once('all_class.php');
										$p = new batch();
										echo $p->dsply_batch();
									?>
								</div>
								<div class="tab-pane" id="about">
									<?php
										include_once('all_class.php');
										$p = new course();
										echo $p->dsply_course();
									?>
								</div>
								<div class="tab-pane" id="content">
									<?php
										include_once('all_class.php');
										$p = new student();
										echo $p->dsply_stdnt();
									?>
								</div>
								<div class="tab-pane" id="contact">
									We are student of IDB IT  Scholarship
								</div>
							</div>
						</div>
					</div>
				</section>
				<footer class="footer_area">
					<div class="row">
						<div class="col-md-12">
							<p>&copy; 2015 All Right Reserved by IDB BISEW</p>
						</div>
					</div>
				</footer>
			</div>
		</div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/bootstrap.min.js"></script>

        
    </body>
</html>
