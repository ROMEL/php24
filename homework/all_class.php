<?php
	class student	{
		private $id;
		private $name;
		private $phone;
		
		public function Setid($sid)	{
			$this->id = $sid;
		}
		public function Getid()	{
			return $this->id;
		}
		
		public function Setname($sname)	{
			$this->name = $sname;
		}
		public function Getname()	{
			return $this->name;
		}
		
		public function Setphone($sphone)	{
			$this->phone = $sphone;
		}
		public function GetPhone()	{
			return $this->phone;
		}
		
		public function dsply_stdnt()	{
			$s = fopen("stdnt.txt","r");
			while($line=fgets($s,4096))	{
				list($sa,$sb,$sc)=explode("|",$line);
				$this->Setid($sa);
				$this->Setname($sb);
				$this->Setphone($sc);
				return $this->Getid()."<br />".$this->Getname()."<br />".$this->GetPhone()."<br />";
			}
		}
	}
	
	class course	{
		private $c_id;
		private $c_name;
		private $c_duration;
		
		public function Setc_id($sc_id)	{
			$this->c_id = $sc_id;
		}
		public function Getc_id()	{
			return $this->c_id;
		}
		
		public function Setc_name($sc_name)	{
			$this->c_name = $sc_name;
		}
		public function Getc_name()	{
			return $this->c_name;
		}
		
		public function Setc_duration($sc_duration)	{
			$this->c_duration = $sc_duration;
		}
		public function Getc_duration()	{
			return $this->c_duration;
		}
		
		public function dsply_course()	{
			$c = fopen("course.txt","r");
			while($line=fgets($c,4096))	{
				list($ca,$cb,$cc)=explode("|",$line);
				$this->Setc_id($ca);
				$this->Setc_name($cb);
				$this->Setc_duration($cc);
				return $this->Getc_id()."<br />".$this->Getc_name()."<br />".$this->Getc_duration()."<br />";
			}
		}
	}
	
	class batch	{
		private $b_id;
		private $b_name;
		private $b_time;
		
		public function Setb_id($sb_id)	{
			$this->b_id = $sb_id;
		}
		public function Getb_id()	{
			return $this->b_id;
		}
		
		public function Setb_name($sb_name)	{
			$this->b_name = $sb_name;
		}
		public function Getb_name()	{
			return $this->b_name;
		}
		
		public function Setb_time($sb_time)	{
			$this->b_time = $sb_time;
		}
		public function Getb_time()	{
			return $this->b_time;
		}
		
		public function dsply_batch()	{
			$b = fopen("batch.txt","r");
			while($line=fgets($b,4096))	{
				list($ba,$bb,$bc)=explode("|",$line);
				$this->Setb_id($ba);
				$this->Setb_name($bb);
				$this->Setb_time($bc);
				return $this->Getb_id()."<br />".$this->Getb_name()."<br />".$this->Getb_time();
			}
		}
	}
?>