<?php
	class student	{
		private $fle_name;
		private $txt_name;
		private $txt_add;
		private $gender;
		private $Subject;
		private $city;
		
		public function Setfle_name($sfle_name)	{
			$this->fle_name = $sfle_name;
		}
		public function Getfle_name()	{
			return $this->fle_name;
		}
		
		public function Settxt_name($stxt_name)	{
			return $this->txt_name = $stxt_name;
		}
		public function Gettxt_name()	{
			return $this->txt_name;
		}
		
		public function Settxt_add($stxt_add)	{
			return $this->txt_add = $stxt_add;
		}
		public function Gettxt_add()	{
			return $this->txt_add;
		}
		
		public function Setgender($sgender)	{
			return $this->gender = $sgender;
		}
		public function Getgender()	{
			return $this->gender;
		}
		
		public function SetSubject($sSubject)	{
			return $this->Subject = $sSubject;
		}
		public function GetSubject()	{
			return $this->Subject;
		}
		
		public function Setcity($scity)	{
			return $this->city = $scity;
		}
		public function Getcity()	{
			return $this->city;
		}
	}
?>