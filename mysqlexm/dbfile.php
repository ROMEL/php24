<?php
$mysqli=new mysqli("localhost","root","");
$dbdrop="drop database exam24";
$rst=$mysqli->query($dbdrop);

$db="create database exam24";
$rst=$mysqli->query($db);
$use="use exam24";
$rst=$mysqli->query($use);

$t1="create table manufacture(id int auto_increment primary key,name varchar(50),address varchar(100),contact_no varchar(50))";
$rst=$mysqli->query($t1);
$tr1="insert into manufacture(name,address ,contact_no) values('IBM','USA','543545')";
$rst=$mysqli->query($tr1);
$tr1="insert into manufacture(name,address ,contact_no) values('Microsoft','USA','543545')";
$rst=$mysqli->query($tr1);

$t2="create table tblproduct(id int auto_increment primary key,pname varchar(50),price float,manufacturer_id int)";
$rst=$mysqli->query($t2);
$tr1="insert into tblproduct(pname,price ,manufacturer_id) values('Laptop',30000,1)";
$rst=$mysqli->query($tr1);
$tr1="insert into tblproduct(pname,price ,manufacturer_id) values('Laptop',30000,2)";
$rst=$mysqli->query($tr1);

$dl="delimiter //";
$rst=$mysqli->query($dl);
$sp="create procedure sp_menu(in nm varchar(50), in address varchar(100),in cno varchar(50)) begin insert into manufacture (name,address,contact_no)values(nm,address,cno); end";
$rst=$mysqli->query($sp);
$tr="create trigger tr_del after delete on manufacture for each row begin delete from tblproduct where manufacturer_id=old.id; end";
$rst=$mysqli->query($tr);
$v="create view vproduct as select id,pname,price,manufacturer_id  from tblproduct where price>=5000";
$rst=$mysqli->query($v);
?>






