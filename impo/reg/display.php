<?php
	include_once("connection.php");
	
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	<a href="form.php">New Entry</a>
	<table border="1px" width="60%">
		<tr>
			<th>Reg No</th>
			<th>Patient Name</th>
			<th>Reg Date</th>
			<th>Father Name</th>
			<th>Gender</th>
			<th>Test</th>
			<th>Doctors</th>
			<th>Modify</th>
		</tr>
		<?php
		$q = "SELECT   reg_id,reg_no,patient_name,reg_date,father_name,gender,test1,test2, doctor.doc_name FROM registration  inner join doctor on registration.doctor=doctor.doc_id";
	$rst = $mysqli->query($q);
			while($row=$rst->fetch_object())	{
		?>
		<tr>
			<td align="center"><?php echo $row->reg_no; ?></td>
			<td align="center"><?php echo $row->patient_name; ?></td>
			<td align="center"><?php echo $row->reg_date; ?></td>
			<td align="center"><?php echo $row->father_name; ?></td>
			<td align="center"><?php echo $row->gender; ?></td>
			<td align="center"><?php echo $row->test1."  ".$row->test2; ?></td>
			<td align="center"><?php echo $row->doc_name; ?></td>
			<td align="center">
				<a href="edit.php?id=<?php echo $row->reg_no; ?>">Edit</a> |
				<a href="delete.php?id=<?php echo $row->reg_no; ?>">Delete</a>
			</td>
		</tr>
		<?php
			}
		?>
	</table>
</body>
</html>