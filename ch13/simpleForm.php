<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
<h1 id="h"></h1>
<form name="myform" action="reg.php" method="post">
<table width="50%" border="0">
  <tr>
    <td>Full name</td>
    <td>:</td>
    <td><input type="text" name="fullname" placeholder="Full Name" required/></td>
  </tr>
  <tr>
    <td>User Name</td>
    <td>:</td>
    <td><input type="text" name="username" placeholder="User Name/login name" required/></td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td><input type="email" name="uemail" placeholder="name@domain.com" required/></td>
  </tr>
  <tr>
    <td>Password</td>
    <td>:</td>
    <td><input type="password" name="upass" placeholder="" required maxlength="20"/></td>
  </tr>
  <tr>
    <td colspan="3">
    <input type="reset" value="Clear"/>
    <input type="submit" name="reg" value="Register"/>
    <input type="button" name="Save" value="Save for later use" onClick="saveForm()"/>
    <input type="button" name="Retrieve" value="Load Saved values" onClick="loadForm()"/>
    </td>
    
  </tr>
</table>
</form>
<script>
document.getElementById("h").innerHTML = "Registration Form";
/*
// Store
localStorage.lastname = "Smith";
// Retrieve
document.getElementById("result").innerHTML = localStorage.lastname;
*/
function saveForm(){
	//alert(document.myform.fullname.value);
	localStorage.fullname = document.myform.fullname.value;
	localStorage.username = document.myform.username.value;
	localStorage.uemail = document.myform.uemail.value;
	localStorage.upass = document.myform.upass.value;	
	}
function loadForm(){
	document.myform.fullname.value = localStorage.fullname;
	document.myform.username.value = localStorage.username;
	document.myform.uemail.value = localStorage.uemail;
	document.myform.upass.value = localStorage.upass;
	}	
</script>
</body>
</html>