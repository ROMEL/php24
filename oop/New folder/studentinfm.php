<?php 

class Courseinfm
{
	private $id;
	private $name;
	private $fees;
	private $duration;
	
	public function SetID($id)
	{
		
		$this->id=$id;
	}
	public function GetID()
	{
		
	return	$this->id;
	}
	public function SetName($name)
	{
		
		$this->name=$name;
	}
	public function GetName()
	{
		
	return	$this->name;
	}
	public function SetFees($fees)
	{
		
		$this->fees=$fees;
	}
	public function GetFees()
	{
		
	return	$this->fees;
	}
	public function SetDuration($duration)
	{
		
		$this->duration=$duration;
	}
	public function GetDuration()
	{
		
	return	$this->duration;
	}
	public function display()
	{
		$b=fopen("student.txt","r");
		while($line=fgets($b,4096))
		{
			list($id,$name,$fees,$duration)=explode("|",$line);
			$this->SetID($id);
			$this->SetName($name);
			$this->SetFees($fees);
			$this->SetDuration($duration);
			return $this->GetID()."<br>".$this->GetName()."<br>".$this->GetFees()."<br>".$this->GetDuration();
		}
	}
}

?>