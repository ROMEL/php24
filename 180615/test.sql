-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2015 at 06:20 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `reg`
--

CREATE TABLE IF NOT EXISTS `reg` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `useremail` varchar(30) NOT NULL,
  `regdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reg`
--

INSERT INTO `reg` (`id`, `username`, `useremail`, `regdate`) VALUES
(1, 'mamun', 'mamun@yahoo.com', '2015-06-16 11:53:19'),
(2, 'Abu Osman', 'Osman_s1986@yahoo.com', '2015-06-16 11:54:51'),
(3, 'yeasin', 'gazi.yeasin@hotmail.com', '2015-06-16 11:55:22'),
(4, 'Romel', 'romelarafath@yahoo.com', '2015-06-16 11:55:40'),
(5, 'munaem', 'munaem.apache@gmail.com', '2015-06-16 11:55:45'),
(6, 'Bangladesh', 'bangladesh@yahoo.com', '2015-06-16 11:55:59'),
(7, 'Manik', 'manik@gmail.com', '2015-06-16 11:56:14'),
(8, 'Rakib', 'rakin@gmail.com', '2015-06-16 11:56:22'),
(9, 'Harun', 'harunar.rashidjnu63@gmail.com', '2015-06-16 11:56:26'),
(10, 'I like your Class', 'abc@yahoo.com', '2015-06-16 11:56:33'),
(11, 'mannan', 'abmannan327@gmail.com', '2015-06-16 11:56:59'),
(16, 'Abdul Mannan', 'abmannan327@mail.com', '2015-06-16 11:59:42'),
(17, 'Rajib Hossain', 'mohammadrajibhossain@gmail.com', '2015-06-16 12:00:05'),
(18, 'Mohammad Maruf Hossain', 'rafsunmaruf@gmail.com', '2015-06-16 12:00:24'),
(19, 'Aktaruzzanan Royhan', 'raihanaktaruzzaman1@gmail.com', '2015-06-16 12:01:24'),
(20, 'mannan', 'abmannan327@gmail.com', '2015-06-16 12:03:59'),
(21, '', '', '2015-06-16 12:04:07'),
(22, '', '', '2015-06-16 12:04:15'),
(23, 'A mannan', 'abmannan327@gimal.com', '2015-06-16 12:13:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reg`
--
ALTER TABLE `reg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reg`
--
ALTER TABLE `reg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
